class HostReviewsController < ApplicationController

  def create
    #check if the reservation exists
    @reservation = Reservation.where(id: host_review_params[:reservation_id],
                                    room_id: host_review_params[:room_id],
                                    user_id: host_review_params[:guest_id]).first

    #check if the current host has already reviewed the guest in this reservation
    if !@reservation.nil?
      @has_reviewed = HostReview.where(reservation_id: host_review_params[:reservation_id],
                                       guest_id: host_review_params[:guest_id]).first
      #if the host hasn't reviewed the guest, allow the host to review
      if @has_reviewed.nil?
        @host_review = current_user.host_reviews.create(host_review_params)
        flash[:success] = "Reviewed created.."
      else
        flash[:success] = "You have reviewed this reservation before."
      end
    else
      flash[:alert] = "This reservation cannot be found."
    end
    redirect_back(fallback_location: request.referer)
  end

  def destroy
    @host_review = Review.find(params[:id])
    @host_review.destroy

    redirect_back(fallback_location: request.referer, notice: "Removed...")
  end

  private
    def host_review_params
      params.require(:host_review).permit(:comment, :star, :room_id, :guest_id, :reservation_id)
    end




end