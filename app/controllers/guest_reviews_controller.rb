class GuestReviewsController < ApplicationController

  def create
    #check if the reservation exists
    @reservation = Reservation.where(id: guest_review_params[:reservation_id],
                                     room_id: guest_review_params[:room_id]
                                     ).first

    #check if the current guest has already reviewed the host in this reservation
    if !@reservation.nil? && @reservation.room.user.id == guest_review_params[:host_id].to_i
      @has_reviewed = GuestReview.where(reservation_id: guest_review_params[:reservation_id],
                                        host_id: guest_review_params[:host_id]).first
      #if the host hasn't reviewed the guest, allow the host to review
      if @has_reviewed.nil?
        @host_review = current_user.guest_reviews.create(guest_review_params)
        flash[:success] = "Reviewed created.."
      else
        flash[:success] = "You have reviewed this reservation before."
      end
    else
      flash[:alert] = "This reservation cannot be found."
    end
    redirect_back(fallback_location: request.referer)
  end

  def destroy
    @guest_review = Review.find(params[:id])
    @guest_review.destroy

    redirect_back(fallback_location: request.referer, notice: "Removed...")
  end

  private
  def guest_review_params
    params.require(:guest_review).permit(:comment, :star, :room_id, :host_id, :reservation_id)
  end




end